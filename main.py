import os
from datetime import datetime
import discord
from discord import Embed, Color
from discord.ext import commands
from keep_alive import keep_alive

bot = commands.Bot(command_prefix='', help_command=None)


@bot.listen('on_message')
async def nicks(ctx):
    if ctx.channel.id == 853665391272263691:
        text = str(ctx.content)
        await ctx.author.edit(nick=text)
        await ctx.delete()


@bot.command()
async def add(ctx, arg1, arg2, arg3, arg4, arg5):
    if ctx.channel.id == 853021995385225216 and arg1 == "casual":
        await ctx.message.delete()
        channel = bot.get_channel(853659020963610665)
        embed = Embed(
            title=arg2,
            color=Color.blurple(),
            url=arg3,
            timestamp=datetime.today(),
            description=arg5,
        )
        embed.set_thumbnail(url=arg4)
        message = await channel.send(embed=embed)
        await message.add_reaction("\U00002B50")
    elif ctx.channel.id == 853021995385225216 and arg1 == "logic":
        await ctx.message.delete()
        channel = bot.get_channel(853658918424543252)
        embed = Embed(
            title=arg2,
            color=Color.blurple(),
            url=arg3,
            timestamp=datetime.today(),
            description=arg5,
        )
        embed.set_thumbnail(url=arg4)
        message = await channel.send(embed=embed)
        await message.add_reaction("\U00002B50")
    elif ctx.channel.id == 853021995385225216 and arg1 == "other":
        await ctx.message.delete()
        channel = bot.get_channel(853664951498571811)
        embed = Embed(
            title=arg2,
            color=Color.blurple(),
            url=arg3,
            timestamp=datetime.today(),
            description=arg5,
        )
        embed.set_thumbnail(url=arg4)
        message = await channel.send(embed=embed)
        await message.add_reaction("\U00002B50")
    elif ctx.channel.id == 853021995385225216 and arg1 == "platform":
        await ctx.message.delete()
        channel = bot.get_channel(853664232548728832)
        embed = Embed(
            title=arg2,
            color=Color.blurple(),
            url=arg3,
            timestamp=datetime.today(),
            description=arg5,
        )
        embed.set_thumbnail(url=arg4)
        message = await channel.send(embed=embed)
        await message.add_reaction("\U00002B50")
    elif ctx.channel.id == 853021995385225216 and arg1 == "rpg":
        await ctx.message.delete()
        channel = bot.get_channel(853658945897103400)
        embed = Embed(
            title=arg2,
            color=Color.blurple(),
            url=arg3,
            timestamp=datetime.today(),
            description=arg5,
        )
        embed.set_thumbnail(url=arg4)
        message = await channel.send(embed=embed)
        await message.add_reaction("\U00002B50")
    elif ctx.channel.id == 853021995385225216 and arg1 == "shooter":
        await ctx.message.delete()
        channel = bot.get_channel(853658990693449728)
        embed = Embed(
            title=arg2,
            color=Color.blurple(),
            url=arg3,
            timestamp=datetime.today(),
            description=arg5,
        )
        embed.set_thumbnail(url=arg4)
        message = await channel.send(embed=embed)
        await message.add_reaction("\U00002B50")
    elif ctx.channel.id == 853021995385225216 and arg1 == "sports":
        await ctx.message.delete()
        channel = bot.get_channel(853664667104444416)
        embed = Embed(
            title=arg2,
            color=Color.blurple(),
            url=arg3,
            timestamp=datetime.today(),
            description=arg5,
        )
        embed.set_thumbnail(url=arg4)
        message = await channel.send(embed=embed)
        await message.add_reaction("\U00002B50")
    elif ctx.channel.id == 853021995385225216 and arg1 == "horror":
        await ctx.message.delete()
        channel = bot.get_channel(853700031260393492)
        embed = Embed(
            title=arg2,
            color=Color.blurple(),
            url=arg3,
            timestamp=datetime.today(),
            description=arg5,
        )
        embed.set_thumbnail(url=arg4)
        message = await channel.send(embed=embed)
        await message.add_reaction("\U00002B50")
    elif ctx.channel.id == 853021995385225216 and arg1 == "nsfw":
        await ctx.message.delete()
        await ctx.send(
            "this channel isn't prepared for this type content but if you want you can just send it in #nsfw-games =] without thumbnail because bot isnt yet implemented there =p "
        )
    else:
        await ctx.message.delete()
        await ctx.send("error!")


@bot.event
async def on_raw_reaction_add(payload):
    if payload.member.bot:
        return
    else:
        if payload.channel_id == 853668278239887391 and payload.message_id == 853692123193933845 and str(
                payload.emoji) == u"\U00002705":
            role = discord.utils.get(bot.get_guild(payload.guild_id).roles,
                                     name="user")
            return await payload.member.add_roles(role)
        elif payload.channel_id == 853665391272263691 and payload.message_id == 853676358319734815 and str(
                payload.emoji) == u"\U0001f7e6":
            role = discord.utils.get(bot.get_guild(payload.guild_id).roles,
                                     name="art")
            return await payload.member.add_roles(role)
        elif payload.channel_id == 853665391272263691 and payload.message_id == 853676358319734815 and str(
                payload.emoji) == u"\U0001f7e9":
            role = discord.utils.get(bot.get_guild(payload.guild_id).roles,
                                     name="mem")
            return await payload.member.add_roles(role)
        elif payload.channel_id == 853665391272263691 and payload.message_id == 853676358319734815 and str(
                payload.emoji) == u"\U0001f7e8":
            role = discord.utils.get(bot.get_guild(payload.guild_id).roles,
                                     name="gmr")
            return await payload.member.add_roles(role)
        elif payload.channel_id == 853665391272263691 and payload.message_id == 853676358319734815 and str(
                payload.emoji) == u"\U0001f7e7":
            role = discord.utils.get(bot.get_guild(payload.guild_id).roles,
                                     name="str")
            return await payload.member.add_roles(role)
        elif payload.channel_id == 853665391272263691 and payload.message_id == 853676358319734815 and str(
                payload.emoji) == u"\U0001f7e5":
            role = discord.utils.get(bot.get_guild(payload.guild_id).roles,
                                     name="dev")
            return await payload.member.add_roles(role)
        elif payload.channel_id == 853665391272263691 and payload.message_id == 853676358319734815 and str(
                payload.emoji) == u"\U00002b1b":
            role = discord.utils.get(bot.get_guild(payload.guild_id).roles,
                                     name="nsfw")
            return await payload.member.add_roles(role)
        else:
            return


# id = 853022915335028787
@bot.command()
async def poll(ctx, *args):
    if ctx.channel.id == 853022130387812373:
        channel = bot.get_channel(853022915335028787)
        if len(args) == 0:
            await ctx.send("?! wat is dat ?!")
        elif len(args) == 1:
            await ctx.send("?! what ?! you didnt said answers !?")
        elif len(args) == 2:
            await ctx.send("ey, you didnt gave second option? how it can be a poll?")
        elif len(args) == 3:
            message = await channel.send(f"**QUESTION: {args[0]}** \n **answer 1:** {args[1]} \n **answer 2:** {args[2]}")
            await message.add_reaction("1️⃣")
            await message.add_reaction("2️⃣")
            await ctx.message.delete()
        elif len(args) == 4:
            message = await channel.send(f"**QUESTION: {args[0]}** \n **answer 1:** {args[1]} \n **answer 2:** {args[2]} \n **answer 3:** {args[3]}")
            await message.add_reaction("1️⃣")
            await message.add_reaction("2️⃣")
            await message.add_reaction("3️⃣")
            await ctx.message.delete()
        elif len(args) == 5:
            message = await channel.send(f"**QUESTION: {args[0]}** \n **answer 1:** {args[1]} \n **answer 2:** {args[2]} \n **answer 3:** {args[3]} \n **answer 4:** {args[4]}")
            await message.add_reaction("1️⃣")
            await message.add_reaction("2️⃣")
            await message.add_reaction("3️⃣")
            await message.add_reaction("4️⃣")
            await ctx.message.delete()
        elif len(args) == 6:
            message = await channel.send(f"**QUESTION: {args[0]}** \n **answer 1:** {args[1]} \n **answer 2:** {args[2]} \n **answer 3:** {args[3]} \n **answer 4:** {args[4]} \n **answer 5:** {args[5]}")
            await message.add_reaction("1️⃣")
            await message.add_reaction("2️⃣")
            await message.add_reaction("3️⃣")
            await message.add_reaction("4️⃣")
            await message.add_reaction("5️⃣")
            await ctx.message.delete()
        elif len(args) == 7:
            message = await channel.send(f"**QUESTION: {args[0]}** \n **answer 1:** {args[1]} \n **answer 2:** {args[2]} \n **answer 3:** {args[3]} \n **answer 4:** {args[4]} \n **answer 5:** {args[5]} \n **answer 6:** {args[6]}")
            await message.add_reaction("1️⃣")
            await message.add_reaction("2️⃣")
            await message.add_reaction("3️⃣")
            await message.add_reaction("4️⃣")
            await message.add_reaction("5️⃣")
            await message.add_reaction("6️⃣")
            await ctx.message.delete()
        elif len(args) == 8:
            message = await channel.send(f"**QUESTION: {args[0]}** \n **answer 1:** {args[1]} \n **answer 2:** {args[2]} \n **answer 3:** {args[3]} \n **answer 4:** {args[4]} \n **answer 5:** {args[5]} \n **answer 6:** {args[6]} \n **answer 7:** {args[7]}")
            await message.add_reaction("1️⃣")
            await message.add_reaction("2️⃣")
            await message.add_reaction("3️⃣")
            await message.add_reaction("4️⃣")
            await message.add_reaction("5️⃣")
            await message.add_reaction("6️⃣")
            await message.add_reaction("7️⃣")
            await ctx.message.delete()
        elif len(args) == 9:
            message = await channel.send(f"**QUESTION: {args[0]}** \n **answer 1:** {args[1]} \n **answer 2:** {args[2]} \n **answer 3:** {args[3]} \n **answer 4:** {args[4]} \n **answer 5:** {args[5]} \n **answer 6:** {args[6]} \n **answer 7:** {args[7]} \n **answer 8:** {args[8]}")
            await message.add_reaction("1️⃣")
            await message.add_reaction("2️⃣")
            await message.add_reaction("3️⃣")
            await message.add_reaction("4️⃣")
            await message.add_reaction("5️⃣")
            await message.add_reaction("6️⃣")
            await message.add_reaction("7️⃣")
            await message.add_reaction("8️⃣")
            await ctx.message.delete()
        elif len(args) == 10:
            message = await channel.send(f"**QUESTION: {args[0]}** \n **answer 1:** {args[1]} \n **answer 2:** {args[2]} \n **answer 3:** {args[3]} \n **answer 4:** {args[4]} \n **answer 5:** {args[5]} \n **answer 6:** {args[6]} \n **answer 7:** {args[7]} \n **answer 8:** {args[8]} \n **answer 9:** {args[9]}")
            await message.add_reaction("1️⃣")
            await message.add_reaction("2️⃣")
            await message.add_reaction("3️⃣")
            await message.add_reaction("4️⃣")
            await message.add_reaction("5️⃣")
            await message.add_reaction("6️⃣")
            await message.add_reaction("7️⃣")
            await message.add_reaction("8️⃣")
            await message.add_reaction("9️⃣")
        elif len(args) == 11:
            message = await channel.send(f"**QUESTION: {args[0]}** \n **answer 1:** {args[1]} \n **answer 2:** {args[2]} \n **answer 3:** {args[3]} \n **answer 4:** {args[4]} \n **answer 5:** {args[5]} \n **answer 6:** {args[6]} \n **answer 7:** {args[7]} \n **answer 8:** {args[8]} \n **answer 9:** {args[9]} \n **answer 10:** {args[10]}")
            await message.add_reaction("1️⃣")
            await message.add_reaction("2️⃣")
            await message.add_reaction("3️⃣")
            await message.add_reaction("4️⃣")
            await message.add_reaction("5️⃣")
            await message.add_reaction("6️⃣")
            await message.add_reaction("7️⃣")
            await message.add_reaction("8️⃣")
            await message.add_reaction("9️⃣")
            await message.add_reaction("🔟")
            await ctx.message.delete()
        elif len(args) > 11:
            await ctx.send("too much arguments =p")
        else:
            return


@bot.event
async def on_raw_reaction_remove(payload):
    if payload.member.bot:
        return
    if payload.channel_id == 853665391272263691 and payload.message_id == 853676358319734815 and str(
            payload.emoji) == u"\U0001f7e6":
        role = discord.utils.get(bot.get_guild(payload.guild_id).roles, name="art")
        return await payload.member.remove_roles(role)
    elif payload.channel_id == 853665391272263691 and payload.message_id == 853676358319734815 and str(
            payload.emoji) == u"\U0001f7e9":
        role = discord.utils.get(bot.get_guild(payload.guild_id).roles, name="mem")
        return await payload.member.remove_roles(role)
    elif payload.channel_id == 853665391272263691 and payload.message_id == 853676358319734815 and str(
            payload.emoji) == u"\U0001f7e8":
        role = discord.utils.get(bot.get_guild(payload.guild_id).roles, name="gmr")
        return await payload.member.remove_roles(role)
    elif payload.channel_id == 853665391272263691 and payload.message_id == 853676358319734815 and str(
            payload.emoji) == u"\U0001f7e7":
        role = discord.utils.get(bot.get_guild(payload.guild_id).roles, name="str")
        return await payload.member.remove_roles(role)
    elif payload.channel_id == 853665391272263691 and payload.message_id == 853676358319734815 and str(
            payload.emoji) == u"\U0001f7e5":
        role = discord.utils.get(bot.get_guild(payload.guild_id).roles, name="dev")
        return await payload.member.remove_roles(role)
    elif payload.channel_id == 853665391272263691 and payload.message_id == 853676358319734815 and str(
            payload.emoji) == u"\U00002b1b":
        role = discord.utils.get(bot.get_guild(payload.guild_id).roles, name="nsfw")
        return await payload.member.remove_roles(role)
    else:
        return


keep_alive()
token = os.environ.get("TOKEN")
bot.run(token)
